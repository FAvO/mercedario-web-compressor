#! /usr/local/bin/python3
# -*- coding: utf-8 -*-
# Author:           Felix A. v. Oertzen (felix@von-oertzen-berlin.de)
# Date:             06/04/2019
# purpose:          #WebCompression
# Version:          0.0.2

# Mercedario Web Compressor - a simple compression tool for speeding up websites
# Copyright (C) 2019 Felix v. Oertzen
# felix@von-oertzen-berlin.de

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


print(
"""
    Mercedario Web Compressor  Copyright (C) 2019  Felix von Oertzen
    This program comes with ABSOLUTELY NO WARRANTY
    This is free software, and you are welcome to redistribute it
    under certain conditions.\n""")


# === IMPORTANWEISUNGEN / IMPORT STATEMENTS ===

import sys
import os
import shutil
import argparse

# === GOBALE KONSTANTEN / GOBAL CONSTANTS ===

TYPICAL_COMMENTS = {
          'html': [['<!--','-->']],
          'xhtml': [['<!--','-->']],
          'htm': [['<!--','-->']],
          'css': [['/*','*/']],
          'php': [['//','\n'],['/*','*/'], ['#','\n']],
          'js': [['//','\n'], ['/*','*/']]
}

# === GLOBALE VARIABLEN / GLOBAL VARIABLES ===

source = ""
destination = ""
silent = False

# === FUNKTIONEN / FUNCTIONS ===

def findMime(mime):
  mt = "application/octet-stream"
  if mime.endswith(".htm") or mime.endswith(".html") or mime.endswith(".php"):
    mt = "text/html";
  if mime.endswith(".css"):
    mt = "text/css";
  if mime.endswith(".js"):
    mt = "text/javascript";
  return mt

def process(mime,source):
  funcs = {
    'text/html'               : processHTML,
    'text/css'                : processCSS,
    'text/javascript'         : processJS,
    'application/x-httpd-php' : processPHP
    }
  if mime in funcs.keys():
    return funcs[mime](source);
  else:
    return source;

def handleFile(path, destination):
  with open(path, mode="rb") as file:
    with open(destination, mode="wb") as output:
      output.write(bytes(process(findMime(path),
                                 file.read().decode("utf-8")), "utf-8"));
      output.close();
    file.close()

def processPath(rootPath):
  if os.path.exists(rootPath):
    new = os.path.join(
                os.path.realpath(destination),
                rootPath[len(origin)+1:]) if rootPath[len(origin)+1:] != "" else os.path.realpath(destination);
    if os.path.isdir(rootPath):
      if not(os.path.exists(new)):
        os.makedirs(os.path.realpath(new));
      for path in os.listdir(rootPath):
        processPath( os.path.realpath(os.path.join(rootPath, path)) );
    else:
      if findMime(new) == "application/octet-stream":
        if not(silent):
          print("Copying", new, "...")
        shutil.copy2(os.path.realpath(rootPath), new);
      else:
        if not(silent):
          print("Compressing", new, "...")
        handleFile(os.path.realpath(rootPath), new);

def processJS(source):
  source = removeShortComment(source, "//");
  source = removeLongComment(source, "/*", "*/");
  newS = ""
  for x in source.splitlines():
    if not(x.strip() == ""):
      newS = newS + x + "\n";
  return removeWhiteSpaces(newS);

def processPHP(source):
  source = removeShortComment(source, "#");
  source = removeShortComment(source, "//");
  source = removeLongComment(source, "/*", "*/");
  source = removeLineBreaks(source);
  return source;
  
def processCSS(source):
  source = removeLongComment(source, "/*", "*/");
  source = removeSpaces(source);
  source = source.replace(" {", "{");
  source = source.replace("{ ", "{");
  source = source.replace(" }", "}");
  source = source.replace("} ", "}");
  source = source.replace(": ", ":");
  source = source.replace(" :", ":");
  source = source.replace(" ;", ";");
  source = source.replace("; ", ";");
  return source;

def processHTML(source):
  # Delete Comments
  source = removeLongComment(source, "<!--", "-->")
  # search for embedded tags like <script>
  scripts = findBlocks(source, "<script"  , "</script>");
  styles  = findBlocks(source, "<style"   ,  "</style>" );
  php     = findBlocks(source, "<?php"       , "?>");
  for s in scripts:
    source = source.replace(source[s[0]:s[1]], process("text/javascript", source[s[0]:s[1]]), 1)
  for s in styles:
    source = source.replace(source[s[0]:s[1]], process("text/css", source[s[0]:s[1]]), 1)
  for p in php:
    source = source.replace(source[p[0]:p[1]], process("application/x-httpd-php", source[p[0]:p[1]]), 1)

  # Delete all unnecessary white spaces
  source = removeSpaces(source);
  # remove new "> <"
  source = source.replace('> <', '><');
  source = source.replace(' />', '/>');
  source = source.replace('; <', ';<');
  source = source.replace(') <', ')<');
  return source

# === internal functions ===

def removeSpaces(source):
  source = removeLineBreaks(source);
  return removeWhiteSpaces(source);

def removeLineBreaks(source):
  source = source.replace('\r\n',' ')
  source = source.replace('\n',' ')
  source = source.replace('\r',' ')
  source = source.replace('\t', ' ')
  return source;

def removeWhiteSpaces(source):
  for i in range(20, 1,-1):
    try:
      source.index(' ' * i)
      source = source.replace(' ' * i, ' ')
    except:
      pass
  return source
  
def findBlocks(source, start, end):
  blocks = []
  beginning = 0
  while True:
    try:
      startPos = source.index(start, beginning)
      try:
        endPos = source.index(end, startPos)
        blocks.append((startPos, endPos));
        beginning = endPos;
      except ValueError:
        break;
    except ValueError:
      break;
  return tuple(blocks)


def removeLongComment(source, start, end):
  blocks = findBlocks(source, start, end);
  if len(blocks) == 0:
    return source;
  newS = ""
  old = 0
  for x in blocks:
    newS = newS + source[old:x[0]]
    old = x[1] + len(end)
  if old != len(source):
    newS = newS + source[old:]
  return newS

def removeShortComment(source, separator):
  short = ""
  for line in source.splitlines():
    end = None
    try:
      end = line.index(separator)
      if line[end-1] in ["\\", "\"", "'"]:#because of ”\//” in js patterns
        end = None;
    except ValueError:
      pass
    short = short + ("" if short == "" else "\n") + line[:end]
  return short


class ErrorParser(argparse.ArgumentParser):
  def error(self, message):
    sys.stderr.write('error: %s\n' % message)
    self.print_help()
    sys.exit(2)


###
# === MAIN PROGRAM ===
###

if __name__ == '__main__':
  parser = ErrorParser(description='A simple web compressor')
  parser.add_argument('-s', '--silent', action='store_true',
                      help="makes the script \"silent\"")
  parser.add_argument('origin', help="the source directory")
  parser.add_argument('destination', nargs="?", help="the destination for saving the compressed page")
  args = parser.parse_args()
  
  silent        = args.silent
  origin        = os.path.realpath(args.origin)
  destination   = os.path.realpath(args.destination if args.destination != None else os.path.realpath(origin)+".compressed" + os.sep);
  print("starting...");
  processPath(origin);
  print("finished...");








